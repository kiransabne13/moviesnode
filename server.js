const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const movies = require('./routes/movies');
const mongoose = require('./config/database');

//connection
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

const app = express();

app.use(logger('dev'));

app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function(req,res){
  res.json({"Kiran" : "Test App"});
});

app.use('/movies', movies);

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
 let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// handle errors
app.use(function(err, req, res, next) {
 console.log(err);

  if(err.status === 404)
   res.status(404).json({message: "Not found"});
  else
    res.status(500).json({message: "Something looks wrong :( !!!"});

});

app.listen(3000, function(){
  console.log('node running on 3000');
});
