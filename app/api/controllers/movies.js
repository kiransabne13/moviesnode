const movieModel = require('../models/movies');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://127.0.0.1:27017/";
module.exports = {

  getById: function(req,res,next){
    console.log(req.body);
    movieModel.findById(req.params.movieId, function(err, movieInfo){
      if (err) {
        next(err);
      } else {
        res.json({status:"success",message:"Got Movie", data:{movies: movieInfo}});
      }
    });
  },

  getAll: function(req,res,next){
    let moviesList = [];

    movieModel.find({}, function(err, movies){
      if (err) {
        next(err);
      } else {
        for (let movie of movies){
          moviesList.push({id: movie._id, name: movie.name, released_on: movie.released_on, rating: movie.rating});
        }
        res.json({status: "success", message: " got all", data: {movies : moviesList}});
      }
    });
  },



  updateById: function(req,res,next){

    MongoClient.connect(url, function (err,db){
      var myquery = { 'name': req.body.name };
      var newvalues = { $set: {'name': req.body.name, 'rating': Number(req.body.rating) } };
      var dbo = db.db("node_rest_api");
      dbo.collection("movies").updateOne(myquery, newvalues, function(err, movieInfo) {

        if (err) {
          next(err);
        } else {
          console.log("1 document updated");
          res.json({status: "success", message: "updated document", data: null});
          db.close();
        }
      });
    });
    console.log(req.body.name);
    console.log(req.body.rating +" type of is "+ typeof(Number(req.body.rating)));

/**
    movieModel.updateOne(myquery, newvalues, function(err, res){
      if (err) throw err;

    });
    */


  },

/*  updateRating: function(req,res,next){}

    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("node_rest_api");
      var myquery = { address: "Valley 345" };
      var newvalues = { $set: {name: "Mickey", address: "Canyon 123" } };
      dbo.collection("customers").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("1 document updated");
        db.close();
      });
    });
  },

*/
  deleteById: function(req, res, next) {
  movieModel.findByIdAndRemove(req.params.movieId, function(err, movieInfo){
   if(err)
    next(err);
   else {
    res.json({status:"success", message: "Movie deleted successfully!!!", data:null});
   }
  });
 },

 create: function(req,res,next){
   movieModel.create({name: req.body.name, released_on: req.body.released_on, rating: req.body.rating}, function(err, result){
     if (err) {
       next(err);
     } else {
       res.json({status: "success", message: "Create Successful", data: null});

     }
   });
 },

}
